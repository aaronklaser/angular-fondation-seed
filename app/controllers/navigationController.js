/**
 * Created by aaronklaser on 1/8/15.
 */
angular.module('myApp').controller('navCtrl', ['$scope', '$location', function($scope, $location) {

    $scope.getClass = function(path) {
        if ($location.path().substr(0, path.length) == path) {
            return "active"
        } else {
            return ""
        }
    }

}]);