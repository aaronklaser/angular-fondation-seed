/**
 * Created by aaronklaser on 1/7/15.
 */
angular.module('myApp').directive('sample', function(){
    return {
        restrict: 'E',
        templateUrl: "templates/sample.html",
        replace: true,
        scope: {

        },
        link: function(scope, element, attrs){
            scope.sampleText = "Hi from directive!";
        }
    };
});