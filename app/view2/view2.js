'use strict';

angular.module('myApp.view2', ['ngRoute'])

.config(['$routeProvider','$locationProvider', function($routeProvider, $locationProvider) {

      $locationProvider.html5Mode(true);
    $routeProvider.when('/view2', {
    templateUrl: 'view2/view2.html',
    controller: 'View2Ctrl'
  });
}])

.controller('View2Ctrl', [function() {

}]);