'use strict';

angular.module('myApp.view1', ['ngRoute'])

.config(['$routeProvider','$locationProvider', function($routeProvider, $locationProvider) {

      $locationProvider.html5Mode(true);

      $routeProvider.when('/view1', {
      templateUrl: 'view1/view1.html',
      controller: 'View1Ctrl'
    });
}])

.controller('View1Ctrl', [function() {

}]);